/**************************************************************************//**
 * @file
 * @brief Use the ADC to take repeated nonblocking measurements on multiple inputs
 * @version 0.0.1
 ******************************************************************************
 * @section License
 * <b>(C) Copyright 2018 Silicon Labs, http://www.silabs.com</b>
 *******************************************************************************
 *
 * This file is licensed under the Silicon Labs Software License Agreement. See
 * "http://developer.silabs.com/legal/version/v11/Silicon_Labs_Software_License_Agreement.txt"
 * for details. Before using this software for any purpose, you must agree to the
 * terms of that agreement.
 *
 ******************************************************************************/
#include "pwm.h"
#include "em_timer.h"
#include "em_gpio.h"
#include "em_cmu.h"
#include "hal-config-sloan.h"
// Note: change this to set the desired duty cycle (used to update CCVB value)
static volatile int dutyCyclePercent = 0;

// Note: change this to set the desired output frequency in Hz
#define PWM_FREQ 520

//--------------------------------------------------------------------------------
// NAME      : TIMER0_IRQHandler
// ABSTRACT  : Interrupt handler for PWM
// ARGUMENTS :
//  none
// RETURN    :
//  none
//--------------------------------------------------------------------------------
void TIMER0_IRQHandler(void)
{
  // Acknowledge the interrupt
  uint32_t flags = TIMER_IntGet(TIMER0);
  TIMER_IntClear(TIMER0, flags);

  // Update CCVB to alter duty cycle starting next period
	if (dutyCyclePercent <= 100)
	{
		TIMER_CompareBufSet(TIMER0, 0, (TIMER_TopGet(TIMER0) * (100 - dutyCyclePercent)) / 100);
	}
}
//--------------------------------------------------------------------------------
// NAME      : set_duty_cycle
// ABSTRACT  : function for setting the % duty cycle
// ARGUMENTS :
//  dutyCycle desired duty cycle - updated on next interrupt
// RETURN    :
//  none
//--------------------------------------------------------------------------------
void set_duty_cycle(int dutyCycle)
{
	TIMER_IntDisable(TIMER0, TIMER_IEN_CC0);
	dutyCyclePercent = dutyCycle;
	TIMER_IntEnable(TIMER0, TIMER_IEN_CC0);
}
//--------------------------------------------------------------------------------
// NAME      : pwm_init
// ABSTRACT  : function initializing PWM
// ARGUMENTS :
//  none
// RETURN    :
//  none
//--------------------------------------------------------------------------------
void pwm_init(void)
{
	// Configure PF6 as PWM output
	GPIO_PinModeSet(PWM_PORT, PWMOUT_PIN, gpioModePushPull, 0);

	  // Enable clock for TIMER0 module
	  CMU_ClockEnable(cmuClock_TIMER0, true);

	  // Configure TIMER0 Compare/Capture for output compare
	  // Use PWM mode, which sets output on overflow and clears on compare events
	  TIMER_InitCC_TypeDef timerCCInit = TIMER_INITCC_DEFAULT;
	  timerCCInit.mode = timerCCModePWM;
	  TIMER_InitCC(TIMER0, 0, &timerCCInit);

	  // Route TIMER0 CC0 to location 30 and enable CC0 route pin
	  // TIM0_CC0 #30 is GPIO Pin PF06
	  TIMER0->ROUTELOC0 |=  TIMER_ROUTELOC0_CC0LOC_LOC30;
	  TIMER0->ROUTEPEN |= TIMER_ROUTEPEN_CC0PEN;

	  // Set top value to overflow at the desired PWM_FREQ frequency
	// The TopSet as well as the timer's prescale value determine the frequency
	// of the PWM signal.  It is computed as follows:
	//
	//  Freq = fclk / ((2^prescale) * TopSet)
	//       = 38400000 / ( (2^1) * (38400000/(520*2)) )
	//       = 520Hz
	// **note: the prescale value represents the value loaded into the
	//         register ... NOT the divider value.  So, for example,
	//           #define timerPrescale2     1
	TIMER_TopSet(TIMER0, CMU_ClockFreqGet(cmuClock_TIMER0) / (PWM_FREQ * 2));

	  // Set compare value for initial duty cycle
	  TIMER_CompareSet(TIMER0, 0, (TIMER_TopGet(TIMER0) * dutyCyclePercent) / 100);

	  // Initialize the timer
	  TIMER_Init_TypeDef timerInit = TIMER_INIT_DEFAULT;
	timerInit.prescale = timerPrescale2;
	  TIMER_Init(TIMER0, &timerInit);

	// Safely enable TIMER0 CC0 interrupt
	TIMER_IntClear(TIMER0, TIMER_IF_CC0);
	NVIC_ClearPendingIRQ(TIMER0_IRQn);

	  // Enable TIMER0 compare event interrupts to update the duty cycle
	TIMER_IntEnable(TIMER0, TIMER_IF_CC0);
	  NVIC_EnableIRQ(TIMER0_IRQn);

	// Initialize static variables
	dutyCyclePercent = 0;
}

