//-----------------------------------------------------------------------------
//   All Rights Reserved
//  Intermatic Incorporated
//  Copyright 2019
//-----------------------------------------------------------------------------
//
//  Description: debug.h contains constants, macros, structure definitions,
//               and public prototypes.
//
//-----------------------------------------------------------------------------
#ifndef __DEBUG_TASK_H__
#define __DEBUG_TASK_H__

#include <stdio.h>
#include "uart.h"

#define DBGS 1 // remove or include from build all debug trace statements
#define LGHT 0
#define DTST 1
#define DBGC 0
#define DEVT 0
#define LGTS 0
#if DBGS
#define DEBUGV(a, ...) {\
if (a) {\
    uint8_t uart_buff[80];\
    int32_t send_count;\
    send_count = sprintf((char*)uart_buff, __VA_ARGS__);\
    uart_put_string((uint8_t*)uart_buff, send_count);\
    }\
}
#else
#define DEBUGV(...)
#endif
/* Imported variables ---------------------------------------------------------*/
/* Imported function prototypes -----------------------------------------------*/


#endif // __DEBUG_TASK_H__


