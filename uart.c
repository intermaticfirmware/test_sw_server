//-----------------------------------------------------------------------------
//   All Rights Reserved
//  Intermatic Incorporated
//  Copyright 2019
//-----------------------------------------------------------------------------
//
//  Description: uart.c is responsible for initializing and setting uart
//  communications.
//  TODO.
//
//-----------------------------------------------------------------------------
#include "em_usart.h"
#include "em_cmu.h"
#include "em_gpio.h"
#include "hal-config-sloan.h"
#include "uart.h"
#include <stdio.h>
#include <stdarg.h>
#include "em_core.h"


static int uart_read_tx_buf (UART_BUFFER_T *p_uart_buf);
UART_BUFFER_T g_uart_buffer;
//--------------------------------------------------------------------------------
// NAME      : uart_init
// ABSTRACT  : This function initializes the uart
// ARGUMENTS :
//   Buffer used for uart transmission
// RETURN    :
//   none
//--------------------------------------------------------------------------------
void uart_init(UART_BUFFER_T *p_uart_buffer)
{
	USART_InitAsync_TypeDef init = USART_INITASYNC_DEFAULT;
	p_uart_buffer->tx_head_point = 0;
	p_uart_buffer->tx_tail_point = 0;
	p_uart_buffer->tx_count = 0;

	// Enable oscillator to GPIO and USART0 modules
	CMU_ClockEnable(cmuClock_GPIO, true);
	CMU_ClockEnable(cmuClock_USART0, true);

	// configure UART
	GPIO_PinModeSet( UART_PORT, CLI_TXD, gpioModePushPull, 1 );
	GPIO_PinModeSet( UART_PORT, CLI_RXD, gpioModeInputPull, 0 );

	// Initialize USART asynchronous mode and route pins
	USART_InitAsync(USART0, &init);
	//LEUART0->ROUTEPEN  = LEUART_ROUTEPEN_RXPEN | LEUART_ROUTEPEN_TXPEN;
	//LEUART0->ROUTELOC0 = LEUART_ROUTELOC0_RXLOC_LOC0 | LEUART_ROUTELOC0_TXLOC_LOC0;

	USART0->ROUTEPEN |= USART_ROUTEPEN_RXPEN | USART_ROUTEPEN_TXPEN;
	USART0->ROUTELOC0 = USART_ROUTELOC0_RXLOC_LOC15 | USART_ROUTELOC0_TXLOC_LOC15;

	//Initialize USART Interrupts
	USART_IntEnable(USART0, USART_IEN_RXDATAV);
	USART_IntEnable(USART0, USART_IEN_TXC);

	//Enabling USART Interrupts
	NVIC_EnableIRQ(USART0_RX_IRQn);
	NVIC_EnableIRQ(USART0_TX_IRQn);
}
//--------------------------------------------------------------------------------
// NAME      : uart_debug
// ABSTRACT  : This function works like a printf function
// ARGUMENTS :
//   printf style
// RETURN    :
//   none
//--------------------------------------------------------------------------------
#if 1
void uart_debug(const char *fmt, ...)
{
	int32_t buf_count;
	int32_t send_count;

	va_list va;
	char buffer[200];
	va_start(va, fmt);
	buf_count = vsprintf(buffer, fmt, va);
	if (buf_count > 0)
		send_count = uart_put_string((uint8_t *)buffer, buf_count);
	if (send_count != buf_count)
		buf_count = 0;
	va_end(va);
}
#endif
//--------------------------------------------------------------------------------
// NAME      : uart_put_string
// ABSTRACT  : This function puts a string into a buffer
// ARGUMENTS :
// RETURN    :
//   int     : returns number of characters added to buffer
//--------------------------------------------------------------------------------
int32_t uart_put_string(uint8_t *buffer, int32_t num_char)
{
	uint8_t *p_buffer = buffer;
	uint8_t ch;
	int32_t num_tx;

	UART_BUFFER_T *p_uart_buf;
	int32_t send_count = 0;

	CORE_DECLARE_IRQ_STATE;

	p_uart_buf = &g_uart_buffer;

	for ( num_tx = 0; num_tx < num_char; num_tx++)
	{
		/* stop before over running the buffer */
		if (p_uart_buf->tx_count >= TXBUFFSIZE)
		{
			break;
		}
		ch = *p_buffer & 0xff;
		p_uart_buf->tx_buf[p_uart_buf->tx_tail_point] = ch;
		p_uart_buf->tx_tail_point = (p_uart_buf->tx_tail_point+1)%TXBUFFSIZE;
		/* start critical section */
		CORE_ENTER_ATOMIC();
		p_uart_buf->tx_count++;
		/* end of critical section */
		CORE_EXIT_ATOMIC();
		send_count++;
		p_buffer++;
	}
    /* enable USART interrupt */
	USART_IntSet(USART0, USART_IFS_TXC);
    return (send_count);

}
//--------------------------------------------------------------------------------
// NAME      : uart_read_tx_buf
// ABSTRACT  : This function reads a character from the buffer and puts it into
//             the transmit buffer
// ARGUMENTS :
// RETURN    :
//   int     : returns the number of characters in the buffer
//--------------------------------------------------------------------------------
static int uart_read_tx_buf (UART_BUFFER_T *p_uart_buf)
{
	if (p_uart_buf->tx_count == 0)
		return 0;

	USART_Tx(USART0, p_uart_buf->tx_buf[p_uart_buf->tx_head_point]);
	p_uart_buf->tx_head_point = (p_uart_buf->tx_head_point+1)%TXBUFFSIZE;
	p_uart_buf->tx_count--;
	return(p_uart_buf->tx_count);
}
//--------------------------------------------------------------------------------
// NAME      : USART0_TX_IRQHandler
// ABSTRACT  : Interrupt handler for transmitting on UART
// ARGUMENTS :
//   none
// RETURN    :
//   none
//--------------------------------------------------------------------------------
void USART0_TX_IRQHandler(void)
{
  uint32_t flags;
  flags = USART_IntGet(USART0);
  //USART_IntClear(USART0, flags);
  UART_BUFFER_T *p_uart_buf = &g_uart_buffer;

  if (flags & USART_IF_TXC)
  {
    if (uart_read_tx_buf(p_uart_buf) == 0)
    {
    	/* clear USART interrupt */
    	USART_IntClear(USART0, flags);
    }
  }
}
