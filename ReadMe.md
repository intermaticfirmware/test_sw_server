Test_SW_Server

This repo contains the test software for the Sloan project:

- reads the adc, jumper settings, motion input and displays on the cli

- cli - uses 115200 baud 8 data bits, 1 stop bit, no parity, no flow control

- contains the Generic On/Off Server Bluetooth Mesh model 

- project uses the following tools:

	Simplicity Studio v4

	GNU ARM Toolchain (v7.2.2017.q4)
	  
	Bluetooth Mesh ADK	1.1.2.0
	Bluetooth Mesh SDK 1.3.4.0
	Bluetooth SDK 2.10.1.0

to provision this device use the SiliconLabs Bluetooth Mesh application found on the google play store


more info can be found at:

N:\Product Engineering\Projects\Active Projects\Sloan New\SW\test software