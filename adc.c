/**************************************************************************//**
 * @file
 * @brief Use the ADC to take repeated nonblocking measurements on multiple inputs
 * @version 0.0.1
 ******************************************************************************
 * @section License
 * <b>(C) Copyright 2018 Silicon Labs, http://www.silabs.com</b>
 *******************************************************************************
 *
 * This file is licensed under the Silicon Labs Software License Agreement. See
 * "http://developer.silabs.com/legal/version/v11/Silicon_Labs_Software_License_Agreement.txt"
 * for details. Before using this software for any purpose, you must agree to the
 * terms of that agreement.
 *
 ******************************************************************************/

#include "adc.h"
#include <stdio.h>
#include "em_device.h"
#include "em_chip.h"
#include "em_cmu.h"
#include "em_adc.h"

#define ADCFREQ   16000000

#define NUM_INPUTS  2

static uint32_t inputs[NUM_INPUTS];

//--------------------------------------------------------------------------------
// NAME      : adc_init
// ABSTRACT  : This function initializes the adc
// ARGUMENTS :
//   none
// RETURN    :
//   none
//--------------------------------------------------------------------------------
#if 0
void adc_init(void)
{
	  // ADC setup
	  // Enable ADC0 clock
	  CMU_ClockEnable(cmuClock_ADC0, true);

	  // Declare init structs
	  ADC_Init_TypeDef init = ADC_INIT_DEFAULT;
	  ADC_InitSingle_TypeDef initSingle = ADC_INITSINGLE_DEFAULT;

	  // Modify init structs and initialize
	  init.prescale = ADC_PrescaleCalc(ADCFREQ, 0); // Init to max ADC clock for Series 1

	  initSingle.diff       = false;        // single ended
	  initSingle.reference  = adcRef2V5;    // internal 2.5V reference
	  initSingle.resolution = adcRes12Bit;  // 12-bit resolution
	  initSingle.acqTime    = adcAcqTime4;  // set acquisition time to meet minimum requirement

	  // Select ADC input. See README for corresponding EXP header pin.
	  initSingle.posSel = adcPosSelAPORT2XCH23;

	  ADC_Init(ADC0, &init);
	  ADC_InitSingle(ADC0, &initSingle);
}
#else
void adc_init(void)
{
	  // Enable ADC0 clock
	  CMU_ClockEnable(cmuClock_ADC0, true);

	  // Declare init structs
	  ADC_Init_TypeDef init = ADC_INIT_DEFAULT;
	  ADC_InitScan_TypeDef initScan = ADC_INITSCAN_DEFAULT;

	  // Modify init structs
	  init.prescale   = ADC_PrescaleCalc(ADCFREQ, 0);

	  initScan.diff       = 0;            // single ended
	  initScan.reference  = adcRef2V5;    // internal 2.5V reference
	  initScan.resolution = adcRes12Bit;  // 12-bit resolution
	  initScan.acqTime    = adcAcqTime4;  // set acquisition time to meet minimum requirement
	  initScan.fifoOverwrite = true;      // FIFO overflow overwrites old data

	  // Select ADC input. See README for corresponding EXP header pin.
	  // Add VDD to scan for demonstration purposes
	  ADC_ScanSingleEndedInputAdd(&initScan, adcScanInputGroup0, adcPosSelAPORT2XCH23);
	  ADC_ScanSingleEndedInputAdd(&initScan, adcScanInputGroup1, adcPosSelAVDD);

	  // Set scan data valid level (DVL) to 2
	  ADC0->SCANCTRLX |= (NUM_INPUTS - 1) << _ADC_SCANCTRLX_DVL_SHIFT;

	  // Clear ADC Scan fifo
	  ADC0->SCANFIFOCLEAR = ADC_SCANFIFOCLEAR_SCANFIFOCLEAR;

	  // Initialize ADC and Scan
	  ADC_Init(ADC0, &init);
	  ADC_InitScan(ADC0, &initScan);

	  // Enable Scan interrupts
	  ADC_IntEnable(ADC0, ADC_IEN_SCAN);

	  // Enable ADC interrupts
	  NVIC_ClearPendingIRQ(ADC0_IRQn);
	  NVIC_EnableIRQ(ADC0_IRQn);
}
#endif
//--------------------------------------------------------------------------------
// NAME      : get_adc_value
// ABSTRACT  : This function allows user to get the latest adc reading
// ARGUMENTS :
//   channel - channel to get the current value
// RETURN    :
//   returns scaled adc value
//--------------------------------------------------------------------------------
int get_adc_value(ADC_CHAN_TYPE_E channel)
{
	int32_t temp_value;

	switch(channel)
	{
	case CURRENT_ADC_CHAN:
		temp_value = inputs[channel];
		// do scaling here
		break;
	case CURRENT_ADC_VDD:
		temp_value = inputs[channel];
		// do scaling here
		break;
	}

	return(temp_value);
}
//--------------------------------------------------------------------------------
// NAME      : ADC0_IRQHandler
// ABSTRACT  : Interrupt handler for ADC conversions
// ARGUMENTS :
//   none
// RETURN    :
//   none
//--------------------------------------------------------------------------------
void ADC0_IRQHandler(void)
{
  int32_t data, i;
  uint32_t id;

  // Get ADC results
  for(i = 0; i < NUM_INPUTS; i++)
  {
    // Read data from ADC
    data = ADC_DataIdScanGet(ADC0, &id);

    inputs[i] = data;
  }

  // Start next ADC conversion
  ADC_Start(ADC0, adcStartScan);
}
