//-----------------------------------------------------------------------------
//   All Rights Reserved
//  Intermatic Incorporated
//  Copyright 2019
//-----------------------------------------------------------------------------
//
//  Description: adc.h contains constants, macros, structure definitions,
//               and public prototypes.
//
//-----------------------------------------------------------------------------
#ifndef ADC_H_
#define ADC_H_

void adc_init(void);

typedef enum
{
	CURRENT_ADC_CHAN = 0,
	CURRENT_ADC_VDD  = 1
} ADC_CHAN_TYPE_E;

int get_adc_value(ADC_CHAN_TYPE_E channel);

#endif /* ADC_H_ */
